#include <epoxy/gl.h>
#undef GLAPI
#include <gtkmm.h>


class MyGLArea : public Gtk::GLArea
{
    public:
        MyGLArea()
        {
            set_size_request(200, 200);
            signal_realize().connect(sigc::mem_fun(*this, &MyGLArea::on_realize));
            signal_render().connect(sigc::mem_fun(*this, &MyGLArea::on_render));
        }
        

    protected:
        void on_realize()
        {
            //Инициализируем OpenGL контекст
            make_current();
            glViewport(0, 0, get_width(), get_height());
            glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        }

        bool on_render(const Glib::RefPtr<Gdk::GLContext>& /*context*/)

        {
        //Рисуем графику используя OpenGL команды
        glClear(GL_COLOR_BUFFER_BIT);
        glBegin(GL_TRIANGLES);
        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex3f(-0.5f, -0.5f, 0.0f);
        glColor3f(0.0f, 1.0f, 0.0f);
        glVertex3f(0.5f, -0.5f, 0.0f);
        glColor3f(1.0f, 0.0f, 1.0f);
        glVertex3f(0.0f, 0.5f, 0.0f);
        glEnd();
        attach_buffers();
        return true;
        }
};

int main (int argc, char* argv[])
{
    auto app = Gtk::Application::create(argc, argv);
    Gtk::Window window;
    window.set_default_size(400, 400);
    MyGLArea gl_area;
    window.add(gl_area);
    window.show_all();
    return app -> run(window);
}
