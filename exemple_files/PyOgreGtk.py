import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

import ogre
#import ogre.renderer.OGRE as Ogre

class GLAreaWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self)
        self.set_default_size(800, 600)
        self.connect("destroy", self.quit)
        self.vbox = Gtk.VBox() #False, 0
        self.add(self.vbox)
        self.gl_area = Gtk.GLArea()
        self.gl_area.set_size_request(800, 600)
        self.gl_area.set_auto_render(True)
        self.vbox.pack_start(self.gl_area, True, True, 0)
        self.show_all()
        self.gl_area.connect("realize", self.on_realize)
        self.gl_area.connect("render", self.on_render, None)
        
    def on_realize(self, area):
        self.ctx = self.gl_area.get_gl_context()
        self.ctx.make_current()
        ogre_root = ogre.Root()
        ogre_root.restoreConfig()
        #ogre_root.showConfigDialog()
        ogre_render_window = ogre_root.initialise(True)
        ogre_render_window.setDeactivateOnFocusChange(False)
        ogre_render_window.setActive(True)
        self.ogre_render_window = ogre_render_window
        
    def on_render(self, area, context):
        self.ctx.make_current()
        ogre.RenderSystem.getSingleton()._beginFrame()
        ogre.RenderSystem.getSingleton()._endFrame()
        self.gl_area.queue_render()
        
    def quit(self, widget, data=None):
        Gtk.main_quit()
if __name__ == "__main__":
    GLAreaWindow()
    Gtk.main()