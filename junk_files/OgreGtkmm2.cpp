#include <Ogre.h>
#include <gtkmm.h>

class OgreArea: public Gtk::GLArea
{
    public:
    Ogre::Root* root;
    GdkGLContext* con;
    //const Glib::RefPtr<Gdk::GLContext>& context;
    //Gtk::Allocation allocation;
    Ogre::RenderWindow* rw;


        OgreArea()
        {
            root = new Ogre::Root();    
            OgreArea::set_auto_render(true);    
            con = OgreArea::get_gl_context();              
            signal_realize().connect(sigc::mem_fun(*this, &OgreArea::on_realize));
            signal_render().connect(sigc::mem_fun(*this, &OgreArea::on_render));
        };

        ~OgreArea()
        {

        };

    protected:
        void on_realize()
        {
            //con = OgreArea::get_gl_context();
            //OgreArea::make_current();
            con::make_current();
            root = new Ogre::Root();
            Ogre::RenderWindow* rw = root->initialise(true);
            rw->setActive(true);
            
        };

        bool on_render(const Glib::RefPtr<Gdk::GLContext>& context)
        {
            //con = OgreArea::get_gl_context();
            con::make_current();
            //Ogre::RenderSystem::_beginFrame();
            //Ogre::RenderSystem::_endFrame();
            //rw->getSingleton().setAutoUpdated(false);
            OgreArea::queue_render();
        };

};

int main (int argc, char* argv[])
{
    auto app = Gtk::Application::create(argc, argv);
    Gtk::Window window;
    window.set_default_size(1200, 900);
    window.set_title("My Ogre Win");
    OgreArea ogre_area;
    window.add(ogre_area);
    window.show_all();
    return app -> run(window);
}