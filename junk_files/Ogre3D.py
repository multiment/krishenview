#from pyogre import ogre
import ogre as ogre
import gi

from gi.repository import Gtk

gi.require_version("Gtk", "3.0")

class OgreWin(Gtk.GLArea):
    def __init__(self):
        Gtk.GLArea.__init__(self)
        self.set_auto_render(True)