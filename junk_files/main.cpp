#include "GL_window.h"
#include "helloworld.h"
#include <gtkmm/application.h>

int main(int argc, char *argv[])
{
  //Glib::RefPtr<Gtk::Application> 
  auto app = Gtk::Application::create(argc, argv);

  HelloWorld helloworld;

  //Gtk::Window window;
  //window.set_default_size(600, 400);

  return app->run(helloworld);
}