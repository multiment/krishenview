#ifndef GTKMM_HELLOWORLD_H
#define GTKMM_HELLOWORLD_H

#include <gtkmm/button.h>
#include <gtkmm/window.h>
#include <gtkmm/box.h>
#include "GL_window.h"


class HelloWorld : public Gtk::Window 
{
    public:
        HelloWorld();
        virtual ~ HelloWorld();
    
    protected:
        void on_button_clicked();

    //typedef GL_Window w_GLArea;
    Gtk::Button m_button;
    Gtk::Box m_box{Gtk::ORIENTATION_HORIZONTAL, false};
    GL_Window w_GLArea;


    
};

#endif