import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


def click_me_now(button):
    print("Button was clicked!!!")

window = Gtk.Window(title="Hello World Now")
window.set_default_size(800,600)
box = Gtk.Box()
button = Gtk.Button.new_with_label("Click me!")
button.set_size_request(50,50)
glarea = Gtk.GLArea()
button.connect("clicked", click_me_now)
box.pack_start(glarea, True, True, 5)
box.pack_start(button, False, False, 0)

window.add(box)
#window.add(button)
window.show_all()
window.connect("destroy", Gtk.main_quit)
Gtk.main()

