#include <gtkmm.h>
#include <gdkmm/general.h>
#include <gdk/gdkx.h>
#include <Ogre.h>
#include <iostream>
//#include <x11/xlib.h>

class MyOgreArea: public Gtk::GLArea
{
    public:
    Ogre::Root* root;
    Ogre::SceneManager* scnMgr;
    Ogre::Light* light;
    Ogre::SceneNode* lightNode;
    Ogre::SceneNode* camNode;
    Ogre::Camera* cam;
    Ogre::Entity* ent;
    Ogre::SceneNode* node;
    Ogre::RenderWindow* RenderWin;
    Ogre::Viewport* vp;
    Gtk::Allocation allocation;
    Ogre::NameValuePairList params;
    
        MyOgreArea()
        {
            signal_realize().connect(sigc::mem_fun(*this, &MyOgreArea::realize));
            signal_unrealize().connect(sigc::mem_fun(*this, &MyOgreArea::unrealize), false);
            signal_render().connect(sigc::mem_fun(*this, &MyOgreArea::render), false);
        };

        virtual ~MyOgreArea()
        {

        };

    protected:
        void realize()
        {
            MyOgreArea::make_current();
            MyOgreArea::set_auto_render(true);
           
            allocation = get_allocation();
            RenderWin = 0;



            root = new Ogre::Root();
            //root->showConfigDialog();
            root->loadPlugin("/usr/lib/x86_64-linux-gnu/OGRE-1.9.0/RenderSystem_GL.so.1.9.0");
            Ogre::RenderSystem* rs = root->getRenderSystemByName("OpenGL Rendering Subsystem");
            rs->setConfigOption("Full Screen", "No");
            root->setRenderSystem(rs);
            root->initialise(false);
            
            scnMgr = root->createSceneManager(Ogre::ST_GENERIC);

            
            
            params["currentGLContext"] = true;
            params["externalGLControl"] = true;

            GdkWindow* parent = get_window()->gobj();
	        GdkDisplay* display = gdk_window_get_display(parent);
	        GdkScreen* screen = gdk_window_get_screen(parent);

	        Display* xdisplay = GDK_DISPLAY_XDISPLAY(display);
	        Screen* xscreen = GDK_SCREEN_XSCREEN(screen);
	        long unsigned int screen_number = gdk_x11_window_get_xid(parent);
            //XID* xid_parent = GDK_WINDOW_XID(parent);
	        

	        params["parentWindowHandle"] =
				Ogre::StringConverter::toString(static_cast<unsigned int>(screen_number)); //+ ":" +
			RenderWin = root->createRenderWindow("Ogre", allocation.get_width(), allocation.get_height(), false, &params);
            
            
        };

        void unrealize()
        {
        
        };

        bool render(const Glib::RefPtr<Gdk::GLContext>& context)
        {
            //scnMgr = root->createSceneManager();
            MyOgreArea::make_current();
            scnMgr->setAmbientLight(Ogre::ColourValue(0.5, 0.5, 0.5));

            light = scnMgr->createLight("MainLight");
            lightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
            lightNode->setPosition(0, 10, 15);
            lightNode->attachObject(light);

            camNode = scnMgr->getRootSceneNode()->createChildSceneNode();
            camNode->setPosition(0, 0, 15);
            camNode->lookAt(Ogre::Vector3(0, 0, -1), Ogre::Node::TS_PARENT);
            
            cam = scnMgr->createCamera("myCam");
            cam->setNearClipDistance(5); // specific to this sample
            cam->setAutoAspectRatio(true);
            camNode->attachObject(cam);

            
            vp = RenderWin->addViewport(cam);
            vp->setBackgroundColour(Ogre::ColourValue(0.0,0.0,0.0));

            //ent = scnMgr->createEntity("/usr/workspace/krichenview/cube.mesh");
            //node = scnMgr->getRootSceneNode()->createChildSceneNode();
            //node->attachObject(ent);

            
              
            //root->startRendering();

            
            return 0;
        };
};


int main (int argc, char* argv[])
{
    auto app = Gtk::Application::create(argc, argv);
    Gtk::Window window;
    window.set_default_size(1200, 900);
    window.set_title("My Ogre Win");
    MyOgreArea ogre_area;
    window.add(ogre_area);
    window.show_all();
    return app -> run(window);
}