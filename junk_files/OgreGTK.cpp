#include <Ogre.h>
#include <gtkmm.h>
//#include <OgreRTShaderShaderGenerator.h>


class MyGLArea : public Gtk::GLArea
{
    public:
        MyGLArea()
        {
            set_size_request(200, 200);
            signal_realize().connect(sigc::mem_fun(*this, &MyGLArea::on_realize));
            signal_render().connect(sigc::mem_fun(*this, &MyGLArea::on_render));
        }
        

    protected:
        /*
        void on_realize()
        {
            MyGLArea::make_current();

        };
*/

        bool on_render(const Glib::RefPtr<Gdk::GLContext>& context)

        {
        //ctx.initApp();
        Ogre::Root* root = new Ogre::Root();
        //root->loadPlugin("RenderSystem_GL.so.1.10.0");
        root->loadPlugin("/usr/lib/x86_64-linux-gnu/OGRE-1.9.0/RenderSystem_GL.so.1.9.0");
        Ogre::RenderSystem* rs = root->getRenderSystemByName("OpenGL Rendering Subsystem");
        root->setRenderSystem(rs);
        root->initialise(false);
        //Ogre::Root* root = ctx.getRoot();
        Ogre::SceneManager* scnMgr = root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");

        // register our scene with the RTSS
        //Ogre::RTShader::ShaderGenerator* shadergen = Ogre::RTShader::ShaderGeneracd ..

        // without light we would just get a black screen    
        Ogre::Light* light = scnMgr->createLight("MainLight");
        Ogre::SceneNode* lightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
        lightNode->setPosition(0, 10, 15);
        lightNode->attachObject(light);

        // also need to tell where we are
        Ogre::SceneNode* camNode = scnMgr->getRootSceneNode()->createChildSceneNode();
        camNode->setPosition(0, 0, 15);
        camNode->lookAt(Ogre::Vector3(0, 0, -1), Ogre::Node::TS_PARENT);

        // create the camera
        Ogre::Camera* cam = scnMgr->createCamera("myCam");
        cam->setNearClipDistance(5); // specific to this sample
        cam->setAutoAspectRatio(true);
        camNode->attachObject(cam);

        // and tell it to render into the main window
        //ctx.getRenderWindow()->addViewport(cam);

        // finally something to render
        Ogre::Entity* ent = scnMgr->createEntity("Sinbad.mesh");
        Ogre::SceneNode* node = scnMgr->getRootSceneNode()->createChildSceneNode();
        node->attachObject(ent);
        //ctx.getRoot()->startRendering();
        //ctx.closeApp();
    //! [main]
        return 0;
        };
};

int main (int argc, char* argv[])
{
    auto app = Gtk::Application::create(argc, argv);
    Gtk::Window window;
    window.set_default_size(400, 400);
    MyGLArea gl_area;
    window.add(gl_area);
    window.show_all();
    return app -> run(window);
}
