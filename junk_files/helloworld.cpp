#include "helloworld.h"
#include "GL_window.h"
#include <iostream>

HelloWorld::HelloWorld()
//:m_button ("Новое окно с кнопкой")
{
    set_border_width(16);
    set_default_size(1000, 800);

    //GL_Window w_GLArea;
    m_button.set_label("Кнопка запуска");
    m_button.set_size_request(100, 50); //Минимальный размер кнопки
    //m_button.size_allocate(100, 50);

    m_button.signal_clicked().connect(sigc::mem_fun(*this, &HelloWorld::on_button_clicked));

    w_GLArea.set_size_request(900,700); //Минимальный размер GlArea. 
    //Метод set_size_request относится к классу Widget (примеры: GLArea, Button)

    m_box.pack_start(w_GLArea);
    m_box.pack_start(m_button);
    

    add(m_box);
    show_all();
}



HelloWorld::~HelloWorld()
{

}

void HelloWorld::on_button_clicked() 
{
    
    //w_GLArea.show();
    std::cout << "Я запустился" << std::endl;
}