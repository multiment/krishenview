
FROM ubuntu:noble@sha256:7de400b33c08cb374fa075e379dfaa2f7eaec9dc6a9c915c21f93a53b90f4227
#FROM gitpod/workspace-base
#FROM gitpod/workspace-C
#FROM buildpack-deps:jammy

COPY install-packages upgrade-packages /usr/bin/

ARG DEBIAN_FRONTEND=noninteractive

#ENV WINDOW_MANAGER="openbox"

RUN yes | unminimize \
    
        zip \
        unzip \
        bash-completion \
        build-essential \
        ninja-build \
        htop \
        jq \
        less \
        locales \
        man-db \
        nano \
        ripgrep \
        software-properties-common \
        sudo \
        time \
        emacs-nox \
        vim \
        multitail \
        lsof \
        ssl-cert \
        fish \
        zsh \
#    && locale-gen en_US.UTF-8
#&& install-packages \
ENV LANG=en_US.UTF-8

### Update and upgrade the base image ###
#RUN upgrade-packages

### Git ###
RUN apt-get install -y git git-lfs sudo

### Gitpod user ###
# '-l': see https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#user
RUN useradd -l -u 33333 -G sudo -md /home/gitpod -s /bin/bash -p gitpod gitpod \
    # passwordless sudo for users in the 'sudo' group
    && sed -i.bkp -e 's/%sudo\s\+ALL=(ALL\(:ALL\)\?)\s\+ALL/%sudo ALL=NOPASSWD:ALL/g' /etc/sudoers
ENV HOME=/home/gitpod
WORKDIR $HOME
# custom Bash prompt
#RUN { echo && echo "PS1='\[\033[01;32m\]\u\[\033[00m\] \[\033[01;34m\]\w\[\033[00m\]\$(__git_ps1 \" (%s)\") $ '" ; } >> .bashrc

### Gitpod user (2) ###
USER gitpod
# use sudo so that user does not get sudo usage info on (the first) login
RUN sudo echo "Running 'sudo' for Gitpod: success" && \
    # create .bashrc.d folder and source it in the bashrc
    mkdir -p /home/gitpod/.bashrc.d && \
    (echo; echo "for i in \$(ls -A \$HOME/.bashrc.d/); do source \$HOME/.bashrc.d/\$i; done"; echo) >> /home/gitpod/.bashrc

# configure git-lfs
#RUN sudo git lfs install --system

#USER gitpod

#RUN apt-get update && apt-get upgrade -y

RUN sudo apt-get install -y libgtk-3-dev\
    libgtk-4-dev\
    curl\
    libgtkmm-3.0-dev\
    libgtkmm-4.0-dev\
    libgirepository1.0-dev\
    python3-dev\
    python3-gi-cairo\
    gir1.2-gtk-3.0\
    gir1.2-gtk-4.0\
    scons\
    mesa-common-dev\
    mesa-utils\
    libepoxy-dev\
    libglm-dev\
    gdb 
#RUN pip install PyGObject PyOpenGL numpy

RUN sudo apt-get install -y xvfb x11vnc xterm openjfx libopenjfx-java openbox \
            emacs-gtk

USER root

# Change the default number of virtual desktops from 4 to 1 (footgun)
RUN sed -ri "s/<number>4<\/number>/<number>1<\/number>/" /etc/xdg/openbox/rc.xml

# Install novnc
RUN git clone --depth 1 https://github.com/novnc/noVNC.git /opt/novnc \
    && git clone --depth 1 https://github.com/novnc/websockify /opt/novnc/utils/websockify
COPY novnc-index.html /opt/novnc/index.html

# Add VNC startup script
COPY start-vnc-session.sh /usr/bin/
RUN chmod +x /usr/bin/start-vnc-session.sh

# This is a bit of a hack. At the moment we have no means of starting background
# tasks from a Dockerfile. This workaround checks, on each bashrc eval, if the X
# server is running on screen 0, and if not starts Xvfb, x11vnc and novnc.
RUN echo "export DISPLAY=:0" >> ~/.bashrc
RUN echo "[ ! -e /tmp/.X0-lock ] && (/usr/bin/start-vnc-session.sh &> /tmp/display-\${DISPLAY}.log)" >> ~/.bashrc

### checks ###
# no root-owned files in the home directory
RUN notOwnedFile=$(find . -not "(" -user gitpod -and -group gitpod ")" -print -quit) \
    && { [ -z "$notOwnedFile" ] \
        || { echo "Error: not all files/dirs in $HOME are owned by 'gitpod' user & group"; exit 1; } }

#USER gitpod

RUN sudo rm -rf /var/lib/apt/lists/*
###
