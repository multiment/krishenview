#include "Mesh.h"
#undef GLAPI
#include <epoxy/gl.h>

Mesh::Mesh(vector<Vertex> *vertices, vector<unsigned int> *indices, vector<Texture> *texture)
{
    this->vertices = vertices;
    this->indices = indices;
    this->texture = texture;

    init_Mesh();
};

Mesh::init_Mesh()
{
    glGenVertexArrays(1, &VAO); //Создаем 1 объект массива вершин
    glGenBuffers(1, &VBO); //Создаем 1 объект буфера вершин
    glGenBuffers(1, &EBO); //Создаем 1 объект буфера элементов (индексы вершин)

    glBindVertexArray(VAO); //Привязываем объект массива вершин

    glBindBuffer(GL_ARRAY_BUFFER, VBO); //Привязываем объект VBO к буферу. GL_ARRAY_BUFFER - тип буфера
    //Копируем вершинные данные в буфер (буфер, количество данных(в байтах), самиданные, как часто меняются данные)
    glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(Vertex), &vertices[0], GL_STATIC_DRAW); 
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO); //Привязываем объект EBO к буфферу элементов. GL_ELEMENT_ARRAY_BUFFER - тип буфера
    //Копируем индексы вершин (элементы) в буфер (буфер, количество индексов, сами индексы, как часто меняются индексы)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(0); //Устанавливаем указатель на первый атрибут вершин
    //Координаты вершин (позиция аргумента (индекс?), размер аргумента в шейдере (vec3), тип данных аргумента, нормализация, шаг (расстояние между наборами данных), смещение начала данных в буфере)
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0); 


}