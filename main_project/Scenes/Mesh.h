

struct Vertex{
    glm::vec3 Position; //Координаты вершины
    glm::vec3 Normal; //Вектор нормали
    glm::vec2 TexCoord; //Координаты текстуры
};

struct Texture{
    unsigned int id;
    string type;
};

/*Класс по созданию и управлению Mesh*/
class Mesh:
{
    private:
        vector<Vertex> *vertices; //Вершины меша
        vector<unsigned int> *indices; //Индексы вершин
        vector<Texture> *texture; //Текстура
        //string<Shader> *shader; //Шейдеры


        unsigned int VBO, VAO, EBO;

    protected:

        
        void init_Mesh(); //Инициализация меша (определение буферов для отрисовки)
        //void set_Mesh(); //Установка координат и путей для сетки

    public:
        void Draw(Shader shader);    

        Mesh(vector<Vertex> *vertices, vector<unsigned int> *indices, vector<Texture> *texture);
        Mesh(JsonObj *Mesh_obj);
        ~Mesh();
};