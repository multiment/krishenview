#include "boost/json/src.hpp"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;
using namespace boost::json;

class JsonSettings {
    
    private:
        value st_;
        object obj_;
        std::string* filename_;
    
    public:
        JsonSettings();
        ~JsonSettings();

    value read_file (std::string filename_){
        ifstream file (filename_);
        file >> st_;
        return st_;
    };

    void json_print(value st){
        cout<< st <<endl;
    };
    
};
