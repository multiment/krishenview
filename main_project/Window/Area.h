#include <gtkmm/glarea.h>



class MyArea: public Gtk::GLArea { //Наследуемся от GLArea

    public:
       
        MyArea(); //Конструктор виджета

        
        virtual ~MyArea(); //Деструктор виджета
        
        

    protected:
        /*Объявления функций, принадлежащих виджету, для работы с GL*/
        void print ();
        void realize();
        void unrealize();
        bool render (const Glib::RefPtr<Gdk::GLContext> &context);
        Gtk::GLArea area; //Создаем объект GLArea для работы внутри этого класса
};
