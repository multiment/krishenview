
#include <gtkmm/application.h>
#include "Win.h"
#include "Area.h"

//#include "json_settings.h"

//MyArea *m_Area = nullptr;
//Win *m_Win = nullptr;
//JsonSettings *settings = nullptr;
//Glib::RefPtr<Gtk::Application> app = nullptr;


class InitApp {

    public:
        
        InitApp ();
        ~InitApp ();
        

    protected:
        Glib::RefPtr<Gtk::Application> app = Gtk::Application::create();
        Win m_Win;
        MyArea m_Area;
        //
        //JsonSettings *m_settings = new JsonSettings();

    //void init_all();
    public:
        void run_all();
};
