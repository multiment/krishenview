/*Для сбоки проекта на данном этапе:
g++ main.cpp Win.h Win.cpp Area.h Area.cpp initapp.h initapp.cpp json_settings.h -I boost -o main_2 -lGL `pkg-config gtkmm-3.0 epoxy --cflags --libs`

g++ main.cpp Win.h Win.cpp Area.h Area.cpp initapp.h initapp.cpp triangle_first.cpp -o main_2 -lGL `pkg-config gtkmm-3.0 epoxy --cflags --libs`

Необходимо указывать все файлы.
TODO: Подумать как загружать в Scons всю папку с файлами*/


//#include <gtkmm/application.h>
//#include "Win.h"
//#include "Area.h"
#include "initapp.h"


//В функцию main передается виджет window в котором уже есть инициализированный виджет GLArea. 
int main (int argc, char* argv[])
{
    
    InitApp m;
    m.run_all();

           
    return 0;
    
}